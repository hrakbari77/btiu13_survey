import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import Survey from './views/Survey.vue'


Vue.use(Router)

export default new Router({

  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/survey',
      name: 'survey',
      component : Survey
    }
  ]
})
